<?php
/**
 * @file
 * Provide Drupal githealth worker plugin.
 */

$plugin = array(
  'title' => t('Drupal Githealth'),
  'description' => t('Provide Drupal repository health check worker plugin.'),
  'perform' => 'githealth_perform',
);

/**
 * Perform the job.
 *
 * @param $properties
 *   Associative array of properties defining the job.
 * @return
 *   An array containing a boolean for pass/fail and the result. The result is
 *
 * @see worker_perform()
 */
function githealth_perform(array $properties) {
/*
  if ($errors = worker_syntax($properties)) {
    // Encapsulate the errors so the type can be easily determined.
    return array(FALSE, $errors);
  }
  elseif ($errors === FALSE) {
    // An error occurred and should be treated as a job failure.
    return array(FALSE, worker_log_last());
  }
*/
  // Prepare results arrays
  $map = array_flip(array('critical', 'major', 'minor'));
  $results = array('#total' => array(0, 0, 0));

  // Perform the job
  worker_log('Beginning Git Repository health check.');
  worker_chdir();

  // Use the 'is_core' property to determine whether is core or not, as core
  // has a different tag/branch naming format
  $iscore = (isset($properties['is_drupal_core'])) ? $properties['is_drupal_core'] : FALSE;

  // Retrieve all branches
  $branches = githealth_branches();
  if ($branches === FALSE) {
  	// Error encountered retrieving branches
    worker_log(t('ERROR: ') . t('Error encountered while retrieving list of repository branches.'));
  	return array(FALSE, t('Error encountered while retrieving list of repository branches.'));
  }
  $results['branchlist'] = $branches;

  // Validate branch names
  githealth_check_branchnames($results, $branches, $iscore);

  // Retrieve all tags
  $tags = githealth_tags();
  if ($tags === FALSE) {
    // Error encountered retrieving tags
    worker_log(t('ERROR: ') . t('Error encountered while retrieving list of repository tags.'));
    return array(FALSE, t('Error encountered while retrieving list of repository tags.'));
  }
  $results['taglist'] = $tags;

  // Validate tag names
  githealth_check_tagnames($results, $tags, $iscore);

  // TODO:  Sandbox checks?

  // TODO:  Project checks?

  // If there are no major or critical items, then consider the scan a pass.
  return array($results['#total'][0] + $results['#total'][1] == 0, $results);
}




function githealth_branches() {
  worker_log('Attempting to retrieve branch list.');
  worker_chdir();
  if ($output = worker_execute_output('git branch -a')) {
    $branches = array();
  	foreach($output as $branch) {
  	  // Detect remote HEAD branch, and set within a special key
  	  if (preg_match('|^  remotes/origin/HEAD -> origin/(.*)$|', $branch, $matches)) {
  	    $branches['HEAD'] = $matches[1];
  	  }
  	  // Only add remote branches, to avoid duplicate listings
  	  elseif (preg_match('|^  remotes/origin/(.*)$|', $branch, $matches)) {
  	    $branches[] = $matches[1];
  	  }
  	}
  	return $branches;
  }
  else {
  	// Unable to retrieve branches
  	return FALSE;
  }
}

function githealth_check_branchnames(&$results, $branches, $iscore) {
  // Determine default branch
  $head = (isset($branches['HEAD']) ? $branches['HEAD'] : FALSE);
  unset($branches['HEAD']);
  // Validate repository branch names
  if (count($branches) > 0) {
    worker_log(t('Evaluating !count branch names.', array('!count' => count($branches))));

    // Select a regex to evaluate repository branch names against
    $core_branch_regex = "/^((3|4).)?\d\.x$/";
    $contrib_branch_regex = "/^\d\.x-\d{1,2}\.x$/";
    $regex = ($iscore) ? $core_branch_regex : $contrib_branch_regex;

    // Initialize 'master branch found' flag
    $masterbranch = FALSE;

    // Loop through available branches
    foreach ($branches as $branch) {
      // Set 'master' branch flags
      $ismaster = ($branch == 'master') ? TRUE : FALSE;
      if ($ismaster) { $masterbranch == TRUE; }

      // Evaluate branch names against Drupal's release naming conventions.
      // Release naming conventions are documented at http://drupal.org/node/1015226.
      if (!(preg_match($regex, $branch)) && (!$ismaster)) {
        // Branch name does not match Drupal's naming patterns.
        // Generate log entry
        worker_log(t('MINOR: ') . t('Found non-standard branch name: (@branch)', array('@branch' => $branch)));
        // Create a notification message to be sent back with the results.
        $message = t('The branch name "@branch" does not match Drupal\'s release naming conventions.  See http://drupal.org/node/1015226.',
          array('@branch' => $branch, )
        );
        // Complain if contrib projects are using the core naming pattern.
        if (!$iscore && preg_match($core_branch_regex, $branch)) {
          $message .= ' ' . t('This branch naming format is reserved for Drupal core, and contrib module branches should follow a "MAJOR.x-MINOR.x" pattern as outlined at the link above.');
        }
        else {
          $message .= ' ' . t('Non-standard branch names are allowed within a repository, but can not be used to generate a release on Drupal.org.');
        }
        // Write the notification message to the worker log, and increment counters.
        githealth_results($results, $message, 'branches', 'minor');
      }
    }

    // Using 'master' branch as HEAD
    if ($head == 'master') {
      $message = t('It appears you are working in the "master" branch in git. ');
      $message .= t('Please create a major version branch, as described at http://drupal.org/node/1127732, ');
      $message .= t('and set it as the default branch for your repository, as described at http://drupal.org/node/1659588');

      worker_log(t('MAJOR: ') . $message);
      githealth_results($results, $message, 'branches', 'major');
    }

    // Ensure at least one branch matches the release naming patterns
    if (!((($masterbranch) ? (count($branches) - 1) : count($branches)) > $results['branches']['#total']['minor'])) {
      $message = t('Repository does not contain any branch names matching Drupal\'s release naming conventions.  See http://drupal.org/node/1015226');
      worker_log(t('CRITICAL: ') . $message);
      githealth_results($results, $message, 'branches', 'critical');
    }
  }
  else {
    // No branches found.
    // I don't believe this is a valid outcome, but it will catch a scenario
    // where githealth_branches returns an empty array due to some logic flaw
    // or change in the git branch command output.
    $message = t('No branches found in repository.');
    worker_log(t('CRITICAL: ') . $message);
    githealth_results($results, $message, 'branches', 'critical');
  }
}


function githealth_tags() {
  worker_log(t('Attempting to retrieve tag list.'));
  worker_chdir();
  if ($output = worker_execute_output('git tag -l')) {
  	$tags = array();
  	foreach($output as $tag) {
  	  $tags[] = $tag;
  	}
    return $tags;
  }
  else {
  	// Unable to retrieve tags
  	return FALSE;
  }
}

function githealth_check_tagnames(&$results, $tags, $iscore) {
  // Validate repository tag names
  if (count($tags) > 0) {
    worker_log(t('Evaluating !count tag names.', array('!count' => count($tags))));
    // Select a regex to evaluate repository tag names against
    $core_tag_regex = "/^((3|4).)?\d\.\d{1,2}(-(unstable|alpha|beta|rc|security)(-)?\d{1,2})?$/";
    $contrib_tag_regex = "/^\d\.x-\d{1,2}\.\d{1,2}(-(unstable|alpha|beta|rc)\d)?$/";
    $regex = ($iscore) ? $core_tag_regex : $contrib_tag_regex;

    // Loop through available branches
    foreach ($tags as $tag) {
      // Evaluate tag names against Drupal's release naming conventions.
      // Release naming conventions are documented at http://drupal.org/node/1015226.
      if (!(preg_match($regex, $tag))) {
        // Tag name does not match Drupal's naming patterns.
        // Generate log entry
        worker_log(t('MINOR: ') . t('Found non-standard tag name: (@tag)', array('@tag' => $tag)));
        // Create a notification message to be sent back with the results.
        $message = t('The tag name "@tag" does not match Drupal\'s release naming conventions.  See http://drupal.org/node/1015226',
          array('@tag' => $tag)
        );
        // Complain if contrib projects are using the core naming pattern.
        if (!$iscore && preg_match($core_tag_regex, $tag)) {
          $message .= ' ' . t('This tag naming format is reserved for Drupal core, and contrib module tags should follow a "MAJOR.x-MINOR.x" pattern as outlined at the link above.');
        }
        else {
          $message .= ' ' . t('Non-standard tag names are allowed within a repository, but can not be used to generate a release on Drupal.org.');
        }
        // Write the notification message to the worker log, and increment counters.
        githealth_results($results, $message, 'tags', 'minor');
      }
    }

    // Ensure at least one tag matches the release naming patterns
    if (!(count($tags)) > $results['tags']['#total']['minor']) {
      $message = t('Repository does not contain any tag names matching Drupal\'s release naming conventions.  See http://drupal.org/node/1015226');
      $message .= t('A valid tag will be required before a release can be generated for this project on Drupal.org.');
      worker_log(t('MAJOR: ') . $message);
      githealth_results($results, $message, 'tags', 'major');
    }
  }
  else {
    // No tags found in repository
    // No branches found.
    // I don't believe this is a valid outcome, but it will catch a scenario
    // where githealth_branches returns an empty array due to some logic flaw
    // or change in the git branch command output.
    $message = t('No tags found in repository. ');
    $message = t('A valid tag matching Drupal\'s release naming conventions will be required before a release can be generated for this project on Drupal.org. ');
    $message = t('Drupal\'s release naming conventions can be found at http://drupal.org/node/1015226');
    worker_log(t('MAJOR: ') . $message);
    githealth_results($results, $message, 'tags', 'major');
  }
}

function githealth_results(&$results, $message, $category, $severity = 'minor') {
  $map = array_flip(array('critical', 'major', 'minor'));
  $type = $map[$severity];
  $results['#total'][$type]++;
  $results[$category]['#total'][$type]++;
  $results[$category][] = array(
    'type' => $severity,
    'message' => $message,
  );
  return $results;
}


// worker_branch_list
// worker_tag_list
// worker_is_sandbox
